#include <iostream>
#include <random>
#include <unistd.h>
#include <string>
#include "platform.hpp"
#include "State.hpp"
#include "Agents.hpp"

void runRandomAgent();
void runMonteCarloAgent(int);
void runConcurrentMonteCarloAgent(int);
void runFurtherConcurrentMonteCarloAgent(int);

int main(int argc, char *argv[])
{
    int opt = 0;
    std::string agent_name = "MonteCarloAgent";
    int seed = time(NULL);
    int trial_time = 100;
    double time_limit = 0.01;
    while ((opt = getopt(argc, argv, "A:s:t:")) != -1) {
        switch (opt)
        {
        case 'A':
            agent_name = optarg;
            break;
        
        case 's':
            seed = atoi(optarg);
            break;

        case 't':
            trial_time = atoi(optarg);
            break;
        }
    }

    srand(seed);
    
    if (agent_name == "RandomAgent")
        runRandomAgent();
    else if (agent_name == "MonteCarloAgent")
        runMonteCarloAgent(trial_time);
    else if (agent_name == "ConcurrentMonteCarloAgent")
        runConcurrentMonteCarloAgent(trial_time);
    else if (agent_name == "FurtherConcurrentMonteCarloAgent")
        runFurtherConcurrentMonteCarloAgent(trial_time);
    else
        std::cout << "No such agent! Please try again." << std::endl;

#ifdef _WIN32
    system("pause");
#endif

    return 0;
}


void runRandomAgent()
{
    system("mode con cols=40 lines=15");

    Game game;
    RandomAgent agent;
    game.print();

    while (!game.isDead())
    {
        State state(game);
        char action = agent.getAction(state);
        if (!game.isPossibleArrow(action))
        {
            std::cout << "Warning: Illegal Action !" << std::endl;
            exit(1);
        }
        
        game.Move(action);
        game.RandomAdd();
        game.print();
    }
}


void runMonteCarloAgent(int trial_time)
{
    system("mode con cols=40 lines=15");

    Game game;
    MonteCarloAgent agent;
    agent.setTrialTime(trial_time);
    game.print();

    while (!game.isDead())
    {
        State state(game);
        char action = agent.getAction(state);
        if (!game.isPossibleArrow(action))
        {
            std::cout << "Warning: Illegal Action !" << std::endl;
            exit(1);
        }
        
        game.Move(action);
        game.RandomAdd();
        game.print();
    }
}

void runConcurrentMonteCarloAgent(int trial_time)
{
    system("mode con cols=40 lines=15");

    Game game;
    ConcurrentMonteCarloAgent agent;
    agent.setTrialTime(trial_time);
    game.print();

    while (!game.isDead())
    {
        State state(game);
        char action = agent.getAction(state);
        if (!game.isPossibleArrow(action))
        {
            std::cout << "Warning: Illegal Action !" << std::endl;
            exit(1);
        }
        
        game.Move(action);
        game.RandomAdd();
        game.print();
    }
}

void runFurtherConcurrentMonteCarloAgent(int trial_time)
{
    system("mode con cols=40 lines=15");

    Game game;
    FurtherConcurrentMonteCarloAgent agent;
    agent.setTrialTime(trial_time);
    game.print();

    while (!game.isDead())
    {
        State state(game);
        char action = agent.getAction(state);
        if (!game.isPossibleArrow(action))
        {
            std::cout << "Warning: Illegal Action !" << std::endl;
            exit(1);
        }
        
        game.Move(action);
        game.RandomAdd();
        game.print();
    }
}