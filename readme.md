# Project 5 - Concurrency

Your agent can't wait to use concurrency to get upgraded! Help him out by equipping him with concurrency method you have just learnt.

Attention: This project requires pre-knowledge about concurrency. If you know **nothing** about concurrency, it may be quite hard to get started. If so, you should check `tutorial.zip` for some examples first.

After this project, you should:
- know the basic concept of cuncurrency.
- be able to set up the environment of c++ concurrency in your computer
- know the basic usage of `std::thread` or `std::async`
- use this to improve the agent for 2048

The whole project is based on game 2048. If you are not familiar with this popular game, please refer to [this website](https://play2048.co//).

---
## Files

There are 3 tasks in total.

Files to modify:

    Agents.cpp
    Agents.hpp

Files that should not be modified:

    platform.cpp
    platform.hpp
    State.cpp
    State.hpp
    main.cpp
    thread_test.cpp

Additional files for learning:

    tutorial.zip

---
## Task 1 - Check the environment

Although you have set up the environment for c++, it might still not support `thread`! Please check the environment by compiling and running `thread_test.cpp` before you getting started.

Type

    g++ thread_test.cpp -o thread_test

in the console to compile, and see if you get `thread_test` or `thread_test.exe`.

If it is okay, run (for Windows cmd)

    thread_test.exe

to see the result.

If you get the result below, congratulations! You are prepared to go to the next task!
```
Ping
Pong
Ping
Pong
Ping
Pong
Ping
Pong
Ping
Pong
Congratulations! Your environment is prepared for concurrency programming.
```
Ping and Pong should appear one by one.

For most Linux users (Mac, Ubantu), it should be easy to pass the test. So if get stucked in compilation or runtime, I bet that you are a Windows user!

To solve the problem, try to install `pthread` in MinGW (search `MinGW manager` in your computer), or try some methods on the Internet (for example, [this link](https://blog.csdn.net/qianchenglenger/article/details/16907821)). You are also recommended to search online for solution, since the problem may differ between different situations.

---
## Task 2 - Concurrent Monte Carlo Agent

In this task, you should implement the function `char ConcurrentMonteCarloAgent::getAction(State gameState)` in `Agents.cpp`.

Use the concurrency method to imporve your Monte Carlo Agent! Remember in Monte Carlo Agent, we do an action, simulate the game process for many times, then pick another action. This time we will do the simulation simultaneously!

You should just pick an action, open a new thread for simulation. Without waiting for the answer, pick the next action and open another thread!

Just modify the code in `MonteCarloAgent::getAction`. You can start with the code provided.

Notice that there is a new method [ `double MonteCarloAgent::randomlyPlayGameManyTimes(State gameState, bool nextIsAgent)` ] in this project to help you reduce the workload. You can check it in `Agents.cpp`. Use it in your implementation because we will use it in the next task.

You can use `std::thread` or `std::async`, both are fine. Remember to add the header file in `Agents.hpp`!

When you finish, type

    g++ main.cpp platform.cpp State.cpp Agents.cpp -o main

in the console to compile, and get `main` or `main.exe`.

Then run (for Windows cmd)

    main.exe -A ConcurrentMonteCarloAgent

You can add more arguments:

    -A: choose an agent to play
        type: std::string
        valid range: { RandomAgent, MonteCarloAgent, ConcurrentMonteCarloAgent }
        Default: MonteCarloAgent
    -s: set the random seed
        type: int
        Default: time(NULL)
    -t: set the trial time (does not work for RandomAgent)
        type: int
        Default: 100

**Further discussion:**

1. To what extent does the agent run faster, compared to naive Monte Carlo Agent?
2. Is the output fixed (i.e. no matter how many times played, the output stays the same) after setting the random seed with your implementation? Why?


---
## Task 3 - Further More ...

In this task, you should implement the function `double FurtherConcurrentMonteCarloAgent::randomlyPlayGameManyTimes(State gameState, bool nextIsAgent)` in `Agents.cpp`.

Your agent is still not satisfied! He thinks you can do more to accelerate the process. This time he asks you to improve the function `randomlyPlayGameManyTimes` with concurrency.

You should improve the implementation in `MonteCarloAgent::randomlyPlayGameManyTimes` by using concurrency to play EACH game simultaneously.

When you finish, type

    g++ main.cpp platform.cpp State.cpp Agents.cpp -o main

in the console to compile, and get `main` or `main.exe`.

Then run (for Windows cmd)

    main.exe -A FurtherConcurrentMonteCarloAgent

to see the result. You can add more arguments:

    -A: choose an agent to play
        type: std::string
        valid range: { RandomAgent, MonteCarloAgent, ConcurrentMonteCarloAgent, FurtherConcurrentMonteCarloAgent }
        Default: MonteCarloAgent
    -s: set the random seed
        type: int
        Default: time(NULL)
    -t: set the trial time (does not work for RandomAgent)
        type: int
        Default: 100

**Further discussion:**

1. What do you observe? Does it really run faster than `ConcurrentMonteCarloAgent` in task 2?
2. Why should this happen? Try to figure out the answer by taking a look at the CUP usage when running different agents.
3. Is it feasible to apply concurrency on `MonteCarloAgentWithTimer`? If yes, how?