#include "stdlib.h"
#include <string>
#include <thread>
#include <mutex>
#include <iostream>
#include <unistd.h>
#include <condition_variable>

#include <future>

bool onRightSide;
std::mutex mut;
std::condition_variable data_cond;

void player( bool isRightSidePlayer, std::string message ) {
  for (int i = 0; i < 5; i++)
  {
    std::unique_lock<std::mutex> lk(mut);
    data_cond.wait(lk,[&isRightSidePlayer]{return isRightSidePlayer == onRightSide;});
    std::cout << message << "\n";
    usleep(1000000);
    onRightSide = !onRightSide;
    lk.unlock();
    data_cond.notify_one();
  }
}

void write_message(std::string const& message) {
    std::cout<<message;
}


int main() {
  onRightSide = true;

  std::thread leftPlayer( player, false, std::string("Pong") );
  std::thread rightPlayer( player, true, std::string("Ping") );
  leftPlayer.join();
  rightPlayer.join();

  auto f=std::async(
	    std::launch::deferred, write_message,
        "Congratulations! Your environment is prepared for concurrency programming.");
  f.wait();

  return 0;
}